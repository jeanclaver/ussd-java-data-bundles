/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import dbaccess.dbsrver.datareseller.DataPackage;
import java.util.Enumeration;

/**
 *
 * @author  JcMoutoh
 */
public class AddonMenu {
    private DataPackage selectedPackage;
 
   
    
    public AddonMenu(DataPackage selectedPackage){
        this.selectedPackage=selectedPackage;
    }
    
    
    public String getString(){
        StringBuilder sb = new StringBuilder();

        
        //sb.append("Vous avez choisi ce "+selectedPackage.getPackageType()+" "+selectedPackage.getPackageName()+" pour ce client : "+selectedPackage.getPrice()+" GNF, merci de  confirmer.\n");                                                                                           
        String addonheader = this.selectedPackage.getAddonheader();
        if((addonheader != null) && (!addonheader.trim().equals("")))
            sb.append(""+this.selectedPackage.getAddonheader()+"\n");
//        sb.append("\nAcheter\n"); 
        int addonprice = this.selectedPackage.getAddonprice();
        
        if(addonprice > 0) {
            
            if (selectedPackage.getPackageName().equals("40MB")){
//                sb.append(""+selectedPackage.getAddonheader()+"\n");
            sb.append("1- 60MB a "+selectedPackage.getAddonprice()+"GNF(48H)\n");
            sb.append("2-"+selectedPackage.getPackageName()+" a "+selectedPackage.getPrice()+"GNF(24H)\n");
           // sb.append("3-"+" Promo 500 MB a 3000GNF(24H)\n");
            }
            
       else if (selectedPackage.getPackageName().equals("120MB")){
                        
//                        sb.append(""+selectedPackage.getAddonheader()+"\n");
            sb.append("1- 140MB a "+selectedPackage.getAddonprice()+"GNF(48H)\n");
            sb.append("2-"+selectedPackage.getPackageName()+" a "+selectedPackage.getPrice()+"GNF(24H)\n");
//            sb.append("3-"+" Promo 500 MB a 3000GNF(24H)\n");
                       }
            else if (selectedPackage.getPackageName().equals ("250MB")){
//                        sb.append(""+selectedPackage.getAddonheader()+"\n");
            sb.append("1- 300MB  a "+selectedPackage.getAddonprice()+"GNF(48H)\n");
            sb.append("2-"+selectedPackage.getPackageName()+" a "+selectedPackage.getPrice()+"GNF(24H)\n");
//           sb.append("3-"+" Promo 500 MB a 3000GNF(24H)\n");
                      
                }
            else if (selectedPackage.getPackageName().equals ("500MB")){
//                        sb.append(""+selectedPackage.getAddonheader()+"\n");
            sb.append("1-"+selectedPackage.getPackageName()+" a "+selectedPackage.getAddonprice()+"GNF(48H)\n");
            sb.append("2-"+selectedPackage.getPackageName()+" a "+selectedPackage.getPrice()+"GNF(24H)\n");
//           sb.append("3-"+" Promo 500 MB a 3000GNF(24H)\n");
                      
                }
             
        }
        

         

//        
        sb.append("# Retour  \n"); 

        
        sb.append("Repondez");                    
        
        String str = sb.toString();
                
        return str;
    }
    
}

