/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import dbaccess.dbsrver.TypePackage;
import mtngc.ussd.AbstractBundleSession;
import dbaccess.dbsrver.datareseller.DataPackage;


/**
 *
 * @author JcMoutoh
 */
public class BundleSession  extends AbstractBundleSession {
    private String ussdSessionId;
    
    private String ussdCode;
    private String packageType;
    private String packageName;
    private TypePackage selectedType;
    private DataPackage selectedPackage;
   
    private RequestEnum selection;
    private int step;
    private int inputStep1 ;
    private int inputStep2;
    private int inputStep3 ;
    private int inputStep4 ;
    private int momostep;
    private String datareq;

    /**
     * @return the ussdSessionId
     */
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    /**
     * @param ussdSessionId the ussdSessionId to set
     */
    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

 

    /**
     * @return the selectedSegment
     */
    public DataPackage getSelectedPackage() {
        return selectedPackage;
    }

    /**
     * @param selectedSegment the selectedSegment to set
     */
    public void setSelectedPackage(DataPackage selectedSegment) {
        this.selectedPackage = selectedSegment;
    }
 
       
    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the ussdCode
     */
    public String getUssdCode() {
        return ussdCode;
    }

    /**
     * @param ussdCode the ussdCode to set
     */
    public void setUssdCode(String ussdCode) {
        this.ussdCode = ussdCode;
    }

    /**
     * @return the packageType
     */
    public String getPackageType() {
        return packageType;
    }

    /**
     * @param packageType the packageType to set
     */
    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public int getInputStep1() {
        return inputStep1;
    }

    public void setInputStep1(int inputStep1) {
        this.inputStep1 = inputStep1;
    }

    public int getInputStep2() {
        return inputStep2;
    }

    public void setInputStep2(int inputStep2) {
        this.inputStep2 = inputStep2;
    }

    public int getInputStep3() {
        return inputStep3;
    }

    public void setInputStep3(int inputStep3) {
        this.inputStep3 = inputStep3;
    }

    public int getInputStep4() {
        return inputStep4;
    }

    public void setInputStep4(int inputStep4) {
        this.inputStep4 = inputStep4;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getMomostep() {
        return momostep;
    }

    public void setMomostep(int momostep) {
        this.momostep = momostep;
    }

    public TypePackage getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(TypePackage selectedPackageType) {
        this.selectedType = selectedPackageType;
    }
     /**
     * @return the selection
     */
    public RequestEnum getSelection() {
        return selection;
    }

    /**
     * @param selection the selection to set
     */
    public void setSelection(RequestEnum selection) {
        this.selection = selection;
    }


    public String getDatareq() {
        return datareq;
    }

    public void setDatareq(String datareq) {
        this.datareq = datareq;
    }

    
}
