/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import dbaccess.dbsrver.datareseller.DataPackage;
import java.util.Enumeration;
import mtngc.sms.SMSClient;

/**
 *
 * @author JcMoutoh
 */
public class ConfirmMenu {
    private DataPackage selectedPackage;
    static String CONFIRMER = "1";
    static String ANNULER = "2";
    static String DEACTIVATE = "3";
    static String RETOUR = "#";
   
    
    private int optionNumber = 0;
   
    
    public ConfirmMenu(DataPackage selectedPackage){
        this.selectedPackage=selectedPackage;
    }
    
    
    public String getString(){
        StringBuilder sb = new StringBuilder();
//        String msisdn ="662249089";
        
        //sb.append("Vous avez choisi ce "+selectedPackage.getPackageType()+" "+selectedPackage.getPackageName()+" pour ce client : "+selectedPackage.getPrice()+" GNF, merci de  confirmer.\n");                                                                                           
        String confirmationTxt = null;
        
        if(selectedPackage.getAddonprice()> 0)
        confirmationTxt = selectedPackage.getAddonconfirmationTxt();
        else 
        confirmationTxt = selectedPackage.getConfirmationTxt();
        
        if(optionNumber == 1){
            confirmationTxt = selectedPackage.getAddonconfirmationTxt();
        }
     
        else if(optionNumber == 2){
             confirmationTxt = selectedPackage.getConfirmationTxt();
        }
        else if (optionNumber == 3){
//           
            String str = "PROMO 100% bonus internet disponible uniquement sur  MyMTN. Telechargez gratuitement et profitez en ";
            return str; 
        }
        
         if((confirmationTxt != null) && (!confirmationTxt.trim().equals("")))
            sb.append(confirmationTxt);
        
        sb.append("\nAcheter\n"); 
        String creditTxt = this.selectedPackage.getCreditTxt();
        
        
        if((creditTxt != null) && (!creditTxt.trim().equals(""))){
            creditTxt = creditTxt.trim();
            sb.append("1. Confirmer \n"); 
        }else {
            sb.append("1. Confirmer\n");  
        }
        
         
        String momoTxt = this.selectedPackage.getMoMoTxt();
        
        if((momoTxt != null) && (!momoTxt.trim().equals(""))){
            momoTxt = momoTxt.trim();
            sb.append("2. Annuler \n"); 
        }else {
            sb.append("2. Annuler\n");  
        }
        
        sb.append("# Retour  \n"); 

        
        sb.append("Repondez");                    
        
        String str = sb.toString();
                
        return str;
    }

    public int getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(int optionNumber) {
        this.optionNumber = optionNumber;
    }
//    public String getmsisdn() {
//        return msisdn;
//    }
//
//    public void setmsisdn(String msisdn) {
//        this.msisdn = msisdn;
//    }
   
}

