/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
//import mtngc.bundles.core.*;

import accesspostgresdb.data.DataClient;
import mtngc.databundles.momo.MoMoRequestStore;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import dbaccess.dbsrver.*;
import dbaccess.dbsrver.datareseller.DataDA;
import dbaccess.dbsrver.datareseller.DataPackage;
import ecwclient.ECWChannelEnum;
import ecwclient.ECWEngine;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import ucipclient.*;
//import mtngc.bundles.core.UserOption;
import mtngc.sms.SMSClient;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

/**
 *
 * @author JcMoutoh
 */
public class DataProvisioningEngine {

    String refillExternalData = "DATA_BUNDLES";
    boolean isAddon;
    boolean isMoMo;
    int price = 0;
    String str = null;
    String FreeFlow;
    private static int[] supportedServiceClasses = {0, 1, 2, 3, 4, 6, 15, 30, 20, 28, 19, 102, 31, 40, 66, 103, 45};

    ResponseEnum executeMAPayment(String msisdn, String transactionId, DataPackage option, String externalData1, String ussdCode) {
        ResponseEnum respEnum = ResponseEnum.SUCCESS;
        MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|BundleName:" + option.getPackageName() + "| Fetching Service Class ");

        UCIPClientEngine ucipEngine = new UCIPClientEngine();
        UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionId);
        DataClient dbClient = new DataClient();
        if (getAccountResponse != null) {

            int sc = getAccountResponse.getServiceClass();
            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Service Class=" + sc);
            if (this.IsServiceClassAllowed(sc)) {
                double balance = getAccountResponse.getBalance();
                if (isAddon) {
                    price = option.getAddonprice();
                } else {
                    price = option.getPrice();
                }
                if (price > balance) {
                    respEnum = ResponseEnum.BALANCEINSUFUSANT;
//                    FeedbackMenu strmsg = new FeedbackMenu();
//                    str = strmsg.getInsufficientBalanceText();
//                    SendPOFailSMS(msisdn, option.getPrice());
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|balance account =" + balance);

                } else {
                    MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|" + option.getPackageName().toString() + "|Price: " + option.getPrice() + "|Funds are sufficient");

                    UCIPUpdateBalanceAndDateResponse deductMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, -price, true, new Date(), refillExternalData);
                    int x = deductMAResponse.getResponseCode();
                    if ((x == 0) && (!deductMAResponse.isError())) {
                        double balanceAfter = deductMAResponse.getBalance();
                        MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|" + respEnum.toString() + "|Price: " + option.getPrice() + "|New Balance: " + balanceAfter + "|Success");

                        respEnum = ResponseEnum.SUCCESS;
                        UCIPUpdateRefillProfilIDResponse refillDAResponse = new UCIPUpdateRefillProfilIDResponse();
                        if (isAddon) {

                            refillDAResponse = ucipEngine.RefillProfileID(msisdn, transactionId, option.getAddonkeyword(), refillExternalData);
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|" + option.getPackageType() + "|" + option.getAddonkeyword() + "| Sending UCIP Request.");
                        } else {

                            refillDAResponse = ucipEngine.RefillProfileID(msisdn, transactionId, option.getKeyword(), refillExternalData);
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|" + option.getPackageType() + "|" + option.getKeyword() + "| Sending UCIP Request.");
                        }

                        int y = refillDAResponse.getResponseCode();
                        if ((y == 0) && (!refillDAResponse.isError())) {
                            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|externalData Response: " + refillExternalData);
                            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Refill Profil ResponseCode: " + refillDAResponse.getResponseCode());
                            respEnum = ResponseEnum.SUCCESS;
//                            SendPOSSuccessSMS(msisdn, option.getPrice());
                            //int msisdnInt = Integer.parseInt(msisdn); 
//                            dbClient.insertTransactionDetails(transactionId, msisdn, option.getPackageType(), option.getPackageName(), option.getDuration(), null, option.getPrice(), option.getKeyword(), respEnum.toString(), null, "NO", null);
                        } else {
                            UCIPUpdateBalanceAndDateResponse refillMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionId, +price, true, new Date(), refillExternalData);
                            if ((x == 0) && (!refillMAResponse.isError())) {
                                respEnum = ResponseEnum.ERROR;
                                FeedbackMenu strmsg = new FeedbackMenu();
                                str = strmsg.getErrortxt();
                                // Insertion in the DB

//                                dbClient.insertTransactionDetails(transactionId, msisdn, option.getPackageType(), option.getPackageName(), option.getDuration(), null, option.getPrice(), option.getKeyword(), respEnum.toString(), null, "NO", null);
                            }

                        }
                    }
                }
            } else {
                MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId + "|Service Class=" + sc);
                respEnum = ResponseEnum.NOT_ALLOWED;
//               str ="Vous n avez pas ekigible a ce service , Tapez *200# pour emprunter un pass internet";
                FeedbackMenu strmsg = new FeedbackMenu();
                str = strmsg.getNotAllowedText();
            }
        } else {
            respEnum = ResponseEnum.ERROR;
            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|error:" + getAccountResponse);
            MLogger.Log(this, LogLevel.ALL, "Subscriber:" + msisdn + "|Session:" + transactionId);
        }
        str = "Cher client votre demande d achat internet a ete envoyee, vous recevrez un message de confirmation. Merci de votre fidelite\n";
        return respEnum;

    }

    public String executeMoMoPayment(String msisdn, String transactionId, DataPackage option, String externalData1, String ussdCode) {
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|" + option.getPackageType() + "|" + option.getPackageName() + " Initiating MoMo Payment.");

        //transactionId = "201807141028070";
        if ((option.getKeywordMoMo() == null) && (option.getKeywordMoMo().trim().equals(""))) {
            str = "Mobile money mot-cle non defini. Veuillez composer le 111 pour obtenir de l'aide";
            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|" + option.getPackageType() + "|" + option.getPackageName() + " | " + str);

            return str;
        } else {

            MoMoRequestStore requestStore = MoMoRequestStore.getInstance();
            requestStore.register(transactionId, msisdn, ussdCode, option, isAddon);

            ECWEngine eng = new ECWEngine();
            msisdn = "224" + msisdn;// "224664222412";

            //int amount = 100;
            String keyword = null;
            if (isAddon) {
                eng.execute(msisdn, transactionId, option.getAddonprice(), option.getAddonkeywordmomo(), ECWChannelEnum.DATA_BUNDLES, true);
//                   eng.execute(msisdn, transactionId, option.getAddonPrice(), option.getAddonKeywordMomo());
//                   eng.execute(msisdn, transactionId, option.getAddonPrice(), option.getAddonKeywordMomo());
                keyword = option.getAddonkeywordmomo();
            } else if (isMoMo) {

                requestStore.register(transactionId, msisdn, ussdCode, option, isMoMo);

            } else {
                eng.execute(msisdn, transactionId, option.getPrice(), option.getKeywordMoMo(), ECWChannelEnum.DATA_BUNDLES, false);
//              eng.execute(msisdn, transactionId, option.getPrice(), option.getKeywordMoMo());
                keyword = option.getKeywordMoMo();
            }
            str = "Cher client votre demande d achat internet a ete envoyee, vous recevrez un message de confirmation. Merci de votre fidelite\n";
            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + transactionId + "|" + keyword + "|" + option.getPackageName() + " Initiating MoMo Payment.");
            return str;
        }
    }

//    public String getCurrentSubscription(String msisdn){
//        String ret =  null;
//        MDPClientEngine mdpClient = new MDPClientEngine();
//        MDPSubscriptionResponse resp = null;
//        resp = mdpClient.getCurrentSubscription(msisdn);
//        if(resp != null){
//            ret = resp.getResponseMessage();
//        }
//        
//        return ret;
//    }
    private void SmsNotifcation(String msisdn) {
        msisdn = "224" + msisdn;// "224664222412";
        String msg = " MTN Yello TV! Plus 500 films & Series, + 20 chaines de Télé EN DIRECT sur votre smartphone et SANS CONSOMMER VOTRE PASS! Telechargez sur bit.ly/39Xwnu6 ";
        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("MyMTN", msisdn, msg);

    }

    private boolean IsServiceClassAllowed(int sc) {
        for (int n : supportedServiceClasses) {
            if (n == sc) {
                return true;
            }
        }

        return false;
    }

    private void SendPOSSuccessSMS(String msisdn, int price) {

        String msg = "Vous avez beneficiez de  " + price + " FG de connexion Internet. Tapez *223# pour le solde.";

        String msisdnStr = msisdn;
        //msisdn = "664222545";
        if (!msisdnStr.startsWith("224")) {
            msisdnStr = "224" + msisdnStr;
        }

        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("Internet", msisdnStr, msg);

    }

    public boolean isIsAddon() {
        return isAddon;
    }

    public void setIsAddon(boolean isAddon) {
        this.isAddon = isAddon;
    }
    
     private void SendPOFailSMS(String msisdn, int price) {

        String msg = "Vous n avez pas assez de credit pour acheter ce Pass de  " + price + " FG , Tapez *200# pour emprunter un pass internet";

        String msisdnStr = msisdn;
        //msisdn = "664222545";
        if (!msisdnStr.startsWith("224")) {
            msisdnStr = "224" + msisdnStr;
        }

        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("Internet", msisdnStr, msg);

    }


}
