/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;

import java.util.List;

/**
 *
 * @author Administrateur
 */
public class MenuRenewal {
    
     private RequestEnum requestEnum;
       public MenuRenewal(RequestEnum requestEnum){
        this.requestEnum=requestEnum;
    }
    
    String header = "Choisissez le mode de renouvellement de votre Pass";
    
    public String getString(){
        StringBuilder sb = new StringBuilder();
        sb.append(header+"\n");
        sb.append("\n");
        sb.append("1-Automatique\n");
        sb.append("2-Manuel\n");
        sb.append("3-Annuler renouvellement\n");
        sb.append("# Retour\n");       
        sb.append("\n");
        sb.append("Repondez");
        
        return sb.toString();
    }
    
}
