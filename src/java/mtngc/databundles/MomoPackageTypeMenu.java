/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import java.io.Serializable;
import java.util.*;
import dbaccess.dbsrver.datareseller.*;

import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

/**
 *
 * @author JcMoutoh
 */
public class MomoPackageTypeMenu {
    String header;
    protected String msg; 
    public static List <String> MOMOPACKAGE_TYPES;
    private final static Object _lock = new Object(); 
    
    
    public MomoPackageTypeMenu(String msisdnStr)  {
       init( msisdnStr);
    }
    
    protected void init(String msisdnStr) {
       header ="Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
        
        DataDA dbClient = new DataDA();
        try{
            
            int msisdn = Integer.parseInt(msisdnStr);
            
            if (MOMOPACKAGE_TYPES == null)    
            {
                synchronized(_lock){
                    if (MOMOPACKAGE_TYPES == null)    
                    {
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Loading packages");
                        List <String> packageTypes = dbClient.getMomoPackageTypes();
                        if ((packageTypes != null) && (packageTypes.size() > 0)){
                            MOMOPACKAGE_TYPES = packageTypes; 
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Loaded "+MOMOPACKAGE_TYPES.size()+" packages");
                        }else {               
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|No packages returned");
                        }
                    }
                }
            }else 
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Using "+MOMOPACKAGE_TYPES.size()+" packages");
            
        }catch (Exception e) {
               MLogger.Log(this, e);
        }    
    }
        
    public String getString(){
        StringBuilder sb = new StringBuilder();
        sb.append(header+"\n");
        List <String>  packageTypes = MOMOPACKAGE_TYPES;
        if(packageTypes != null){
            int i=1;
            for(String packageType : packageTypes){
//                sb.append(i+". "+packageType+"\n"); 
                i++;
            }        
//            sb.append(i+". "+"Mon compte\n"); 
            sb.append("#. Retour  \n");
            sb.append("\n");
            sb.append("Repondez");
        }else {
            sb.append("Pardon. Nous avons rencontré une erreur..\n");
            sb.append("\n");
        }
        return sb.toString();
    }
    
    
    
    public String getMomoPackageType(int index){
        String ret = null;
        
        List <String>  packageTypes = MOMOPACKAGE_TYPES;
        
        int i=1;
        for(String packageType : packageTypes){
            if(i == index)
                return packageType;
            i++;
        }       
         
        return ret;
    }
    
    public int getSize(){
        return MOMOPACKAGE_TYPES.size();
    }
    
}
