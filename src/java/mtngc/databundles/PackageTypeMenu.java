/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import java.io.Serializable;
import java.util.*;
import dbaccess.dbsrver.datareseller.*;

import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

/**
 *
 * @author JcMoutoh
 */
public class PackageTypeMenu {
    String header;
    protected String msg; 
    public static List <String> PACKAGE_TYPES;
    private final static Object _lock = new Object(); 
    private int optionNumber = 0;
    
    public PackageTypeMenu(String msisdnStr){
       init( msisdnStr);
    }
    
    protected void init(String msisdnStr) {
        header ="MTN Internet";
        
        DataDA dbClient = new DataDA();
        try{
            
            int msisdn = Integer.parseInt(msisdnStr);
            
            if (PACKAGE_TYPES == null)    
            {
                synchronized(_lock){
                    if (PACKAGE_TYPES == null)    
                    {
                        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Loading packages");
                        List <String> packageTypes = dbClient.getPackageTypes();
                        if ((packageTypes != null) && (packageTypes.size() > 0)){
                            PACKAGE_TYPES = packageTypes; 
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Loaded "+PACKAGE_TYPES.size()+" packages");
                        }else {               
                            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|No packages returned");
                        }
                    }
                }
            }else 
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Using "+PACKAGE_TYPES.size()+" packages");
            
        }catch (Exception e) {
               MLogger.Log(this, e);
        }    
    }
        
    public String getString(){
        StringBuilder sb = new StringBuilder();
        sb.append(header+"\n");
        List <String>  packageTypes = PACKAGE_TYPES;
        if(packageTypes != null){
            int i=1;
            for(String packageType : packageTypes){
                sb.append(i+". "+packageType+"\n"); 
                i++;
            }        
//            sb.append(i+". "+"Mon compte\n"); 
//            sb.append("0. Retour  \n");
            sb.append("\n");
            sb.append("Repondez");
        }
       
        else {
            sb.append("Pardon. Nous avons rencontré une erreur..\n");
            sb.append("\n");
        }
        return sb.toString();
    }
    
    
    
    public String getPackageType(int index){
        String ret = null;
        
        List <String>  packageTypes = PACKAGE_TYPES;
        
        int i=1;
        for(String packageType : packageTypes){
            if(i == index)
                return packageType;
            i++;
        }       
         
        return ret;
    }
    
    public int getSize(){
        return PACKAGE_TYPES.size();
    }
     public int getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(int optionNumber) {
        this.optionNumber = optionNumber;
    }
}
