/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;

import dbaccess.dbsrver.datareseller.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession ;
import javax.servlet.ServletContext;
import java.io.*;
import java.util.*;
import mtngc.sms.SMSClient;
import mtngc.ussd.AbstractBundleSession;
import mtngc.ussd.AbstractMainMenu;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import mtngc.ussd.AbstractUSSDHandler;
import dbaccess.dbsrver.GoodYearMsisdn;
import dbaccess.dbsrver.MonCompte;
import dbaccess.dbsrver.TypePackage;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
//import mtngc.bundles.core.UserOption;
//import mtngc.bundles.core.ProvisioningEngine;
import org.apache.commons.lang.StringUtils;
import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;

/**
 *
 * @author JcMoutoh
 */
public class RequestHandler extends AbstractUSSDHandler {

    static String externalData1;
    String refillExternalData = "DATA_BUNDLES";
    String FreeFlow;
    String str = null;
    private int inputStep1;
    private int inputStep2;
    private int inputStep3;
    private int inputStep4;
    private DataPackage packagesAddon[];
    private TypePackage selectedPackageType;

    public RequestHandler() {

    }

    static {
        ContextKeyName = "data_ussd";
        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            String filePath = "C:\\logs\\" + ContextKeyName + "_v1";
            try {
                File file = new File(filePath);
                if (!file.exists()) {
                    if (file.mkdir()) {
                        System.out.println("Directory is created!");
                    } else {
                        System.out.println("Failed to create directory!");
                    }
                }
            } catch (Exception e) {
            }
            MLogger.setHomeDirectory(filePath);
        } else if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            String filePath = "/Users/macbook/logs/ussd_" + ContextKeyName + "_v1";
            try {
                File file = new File(filePath);
                if (!file.exists()) {
                    if (file.mkdir()) {
                        System.out.println("Directory is created!");
                    } else {
                        System.out.println("Failed to create directory!");
                    }
                }
            } catch (Exception e) {
            }
            MLogger.setHomeDirectory(filePath);

        } else if (System.getProperty("os.name").toLowerCase().contains("nux")) {
            String filePath = "/var/log/ussd_" + ContextKeyName + "_v1";
            try {
                File file = new File(filePath);
                if (!file.exists()) {
                    if (file.mkdir()) {
                        System.out.println("Directory is created!");
                    } else {
                        System.out.println("Failed to create directory!");
                    }
                }
            } catch (Exception e) {
            }
            MLogger.setHomeDirectory(filePath);
        }

    }

    public void handleRequest(HttpServletRequest request, HttpServletResponse response) {
        String msisdnStr = request.getParameter("MSISDN");
        //msisdnStr = "224660629885";
        try {
            msisdnStr = msisdnStr.substring(3);
        } catch (Exception n) {
        };
        //

        handleRequest(msisdnStr, request, response);
    }

    public void handleRequest(String msisdnStr, HttpServletRequest request, HttpServletResponse response) {
        TraceParametersAndHeaders(request);

        try {

            int msisdn = 0;
            try {
                msisdn = Integer.parseInt(msisdnStr);
            } catch (Exception n) {
            };

            if (IsMSISDNAllowed(msisdn)) {
                processRequest(msisdnStr, request, response);
            } else {
                response.setContentType("UTF-8");
                response.setHeader("Freeflow", "FB");
                response.setHeader("cpRefId", "jcmk2544");
                PrintWriter out = response.getWriter();
                out.append("Le service n'est pas disponible");
                out.close();
            }

        } catch (Exception e) {
            MLogger.Log(this, e);
        }
    }

    private void processRequest(String msisdn, HttpServletRequest request, HttpServletResponse response) {
        this.FreeFlow = "FC";
        try {

            String ussdSessionid = request.getParameter("SESSIONID");
            String newrequest = request.getParameter("newrequest");
            String input = request.getParameter("INPUT");

            ServletContext context = request.getServletContext();

            StringBuilder sb = new StringBuilder();

            if (input.endsWith("5000")) {
                //input = "5000";
                String str = processShortCut("1", "1", msisdn, ussdSessionid, context);
                sb.append(str);
            } else if (input.endsWith("10000")) {
                //input = "100000";
                String str = processShortCut("1", "2", msisdn, ussdSessionid, context);
                sb.append(str);
            } else if (input.endsWith("*1000")) {
                //input = "10000";
                String str = processShortCut("2", "1", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input " + input);
            } else if (input.endsWith("*2000")) {
                //input = "2000";
                String str = processShortCut("2", "2", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*3000")) {
                //input = "3000";
                String str = processShortCut("2", "3", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*5500")) {
                //input = "5000";
                String str = processShortCut("3", "1", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*15000")) {
                //input = "15000";
                String str = processShortCut("4", "1", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*30000")) {
                //input = "30000";
                String str = processShortCut("4", "2", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*40000")) {
                //input = "40000";
                String str = processShortCut("5", "1", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*75000")) {
                //input = "75000";
                String str = processShortCut("5", "2", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*110000")) {
                //input = "110000";
                String str = processShortCut("5", "3", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*190000")) {
                //input = "190000";
                String str = processShortCut("5", "4", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*355000")) {
                //input = "355000";
                String str = processShortCut("5", "5", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("*5000")) {
                //input = "5000 Night";
                String str = processShortCut("6", "1", msisdn, ussdSessionid, context);
                sb.append(str);
                MLogger.Log(this, LogLevel.DEBUG, "Processing desactivation input " + input);
            } else if (input.endsWith("90")) {
                //input = "5000";
                String str = processAddonShortCut("6", "3", msisdn, ussdSessionid, context);
                sb.append(str);
            } else if (input.endsWith("91")) {
                //input = "5000";
                String str = processAddonShortCut("6", "4", msisdn, ussdSessionid, context);
                sb.append(str);
            } else if (input.endsWith("92")) {
                //input = "5000";
                String str = processAddonShortCut("6", "5", msisdn, ussdSessionid, context);
                sb.append(str);
            } else if (input.endsWith("93")) {
                //input = "5000";
                String str = processAddonShortCut("6", "6", msisdn, ussdSessionid, context);
                sb.append(str);
            } else {

                MLogger.Log(this, LogLevel.DEBUG, "MSISDN:" + msisdn + "|Session " + ussdSessionid + "| Processing request. STEP =0");

                String contextKey = GetContextKey(ussdSessionid);
                Object obj = context.getAttribute(contextKey);
                if (obj == null) {
                    if (input != null) {
                        String ussdCode = input.trim();
                        String str = displayPackages(context, ussdSessionid, msisdn, ussdCode);
                        sb.append(str);
                    } else {
                        sb.append("Application is running");
                    }
                } else {

                    BundleSession bundleSession = (BundleSession) obj;
                    int step = bundleSession.getStep();
                    String ussdCode = bundleSession.getUssdCode();
                    MLogger.Log(this, LogLevel.DEBUG, "MSISDN:" + msisdn + "|Session " + ussdSessionid + "| Processing request.  STEP =" + step);

                    if (step == 1) {// if previous screen was the main forfait (package type) menu

                        String str = processPackageTypeInput(input, msisdn, ussdSessionid, bundleSession, context);
                        sb.append(str);
                    } else if (step == 2) {// if previous screen was the data bundle menu (bundle name) menu

                        String str = processAddonDataBundleSelectionInput(input, msisdn, ussdSessionid, bundleSession, context);

                        sb.append(str);
                    } else if (step == 3) {// if previous screen was the bundle purchase confirmlation menu

                        //String str = processAddonDataBundleSelectionInput(input,  msisdn,  ussdSessionid, bundleSession, context);  
                        String str = processDataBundleConfirmationInput(input, msisdn, ussdSessionid, bundleSession, context);
                        sb.append(str);
                    } else if (step == 4) {// if previous screen was the bundle purchase confirmlation menu
                        String str = payload(input, msisdn, ussdSessionid, bundleSession, context);
                        sb.append(str);
                    } else if (step == 5) {// if previous screen was the MoMo data bundle menu (bundle name) menu

                        String str = processMoMoDataBundleSelectionInput(input, msisdn, ussdSessionid, bundleSession, context);

                        sb.append(str);
                    } else if (step == 6) {// if previous screen was the bundle purchase confirmlation menu
                        String str = processMoMoDataBundleConfirmInput(input, msisdn, ussdSessionid, bundleSession, context);
                        sb.append(str);
                    } else if (step == 7) {// if previous screen was the bundle purchase confirmlation menu
                        String str = processBundlesMomo(input, msisdn, ussdSessionid, bundleSession, context);
                        sb.append(str);
                    } else if (step == 8) {// if previous screen was the bundle purchase confirmlation menu
                        String str = processMonComptenput(input, msisdn, ussdSessionid, bundleSession, context);
                        sb.append(str);
                    } else if (step == 9) {// if previous screen was the bundle purchase confirmlation menu
                        String str = processMonCompteConfirmInput(input, msisdn, ussdSessionid, bundleSession, context);
                        sb.append(str);
                    } else if (step == 10) {// if previous screen was the PAYG menu
                        String str = processPAYGMenuInput(input, msisdn, ussdSessionid, bundleSession, context);
                        sb.append(str);
                    }
//                     else if (step == 11){// if previous screen was the PAYG menu
//                        String str = processPaygConfirmMenuInput(input,  msisdn,  ussdSessionid, bundleSession, context);  
//                        sb.append(str ); 
//                    }

                }

            }

            response.setContentType("UTF-8");
            response.setHeader("Freeflow", this.FreeFlow);
            response.setHeader("cpRefId", "jcmk2544");

            PrintWriter out = response.getWriter();
//  
            out.append(sb.toString());
            MLogger.Log(this, LogLevel.ALL, sb.toString());
            out.close();

        } catch (Exception e) {
            MLogger.Log(this, e);
        }

    }

    /*
     * The menu to display the packageType
     */
    public String processPackageTypeInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;
        DataDA dbClient = new DataDA();
        String packagetype_name = null;
        String ussdCode = bundleSession.getUssdCode();
        if ((input != null) && (!input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim()))) {

            input = input.trim();
            boolean isEligible = Eligible(msisdn);
//            boolean isMomoBundle = BonusMomoPackage(packagetype_name);
            int packageTypeIndex = Integer.parseInt(input);

            int MomopackageTypeIndex = Integer.parseInt(input);
            inputStep1 = packageTypeIndex;

            bundleSession.setInputStep1(inputStep1);

            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|inputStep1 " + bundleSession.getInputStep1() + "|inputStep2=" + bundleSession.getInputStep2() + "|inputStep3=" + bundleSession.getInputStep3() + "|inputStep4=" + bundleSession.getInputStep4());
            DefaultPackageTypeMenu packageTypeMenu = new DefaultPackageTypeMenu(msisdn);
            String packageTypeName = packageTypeMenu.getPackageType(packageTypeIndex);
            MomoPackageTypeMenu type = new MomoPackageTypeMenu(msisdn);
            String MomopackageTypeName = packageTypeMenu.getPackageType(MomopackageTypeIndex);

            if (packageTypeName.equals("BONUS PASS MOMO")) {
                RequestEnum reqEnum = null;
                reqEnum = RequestEnum.MOMOBONUSDATA;
                bundleSession.setSelection(reqEnum);
                msg = displayMomoPackages(context, ussdSessionid, msisdn, ussdCode);
                String contextKey = GetContextKey(ussdSessionid);
                bundleSession.setStep(5);
                bundleSession.setPackageType(packageTypeName);
                context.setAttribute(contextKey, bundleSession);
//                msg = "Cher, client suite a une mise a jour en cours, vous pouvez temporairement verifier votre solde en tapant *223*20#. Merci pour la comprehension ";
//                SmsNotifcation(msisdn);

            } else if (packageTypeName.equals("Mon Compte")) {

                RequestEnum reqEnum = null;
                reqEnum = RequestEnum.MONCOMPTE;
//                 bundleSession.setSelection(reqEnum);
//                 msg =displayMonCompteMenu(input, msisdn, ussdSessionid, bundleSession, context);
//                 String contextKey = GetContextKey(ussdSessionid);
//                 bundleSession.setStep(9);
//                 bundleSession.setPackageType(packageTypeName);
//                 context.setAttribute(contextKey, bundleSession);
                msg = "Cher, client suite a une mise a jour en cours, vous pouvez temporairement verifier votre solde en tapant *223*20#. Merci pour la comprehension ";
                SmsNotifcation(msisdn);

                this.FreeFlow = "FC";
                this.Cleanup(context, ussdSessionid);

            } else if (packageTypeName.equals("YelloTV")) {

                msg = "Cher client, suite a une mise a jour en cours, vous pouvez continuer a acheter vos Pass Yello TV directement sur l appli ou sur yellotv.com";
                SmsNotifcation(msisdn);

                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);

            } else {
                DefaultPackageNameMenu packageNameMenu = new DefaultPackageNameMenu(msisdn, packageTypeName);

                msg = packageNameMenu.getString();

                String contextKey = GetContextKey(ussdSessionid);
                bundleSession.setStep(2);
                bundleSession.setPackageType(packageTypeName);
                context.setAttribute(contextKey, bundleSession);

            }

        } else {
            //naughty user, he didnt enter anything
            PackageTypeMenu packageTypeMenu = new PackageTypeMenu(msisdn);
            msg = packageTypeMenu.getString();

        }

        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Message " + msg);

        return msg;
    }

    public String processDataBundleSelectionInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;
        StringBuilder sb = new StringBuilder();
        //String ussdCode = bundleSession.getUssdCode();
        String packageTypeName = bundleSession.getPackageType();
        boolean isEligible = Eligible(msisdn);
        if ((input != null) && (!input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim()))) {

            input = input.trim();
            int packageNameIndex = Integer.parseInt(input);
            int addonprice;

            if (isEligible) {
                PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
                DataPackage apackage = packageNameMenu.getPackage(packageTypeName, packageNameIndex);
                addonprice = apackage.getAddonprice();

                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|inputStep1 " + bundleSession.getInputStep1() + "|inputStep2=" + bundleSession.getInputStep2() + "|inputStep3=" + bundleSession.getInputStep3() + "|inputStep4=" + bundleSession.getInputStep4());

                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|processDataBundleSelectionInput=" + apackage.getPackageName() + " ADDON PRICE " + apackage.getAddonprice());

                if (apackage != null) {

                    ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                    msg = confirmMenu.getString();
                    String contextKey = GetContextKey(ussdSessionid);
                    bundleSession.setStep(4);
                    bundleSession.setSelectedPackage(apackage);
                    context.setAttribute(contextKey, bundleSession);

                } else {

                    msg = packageNameMenu.getString();

                }
            } else {
                DefaultPackageNameMenu DefaultpackageNameMenu = new DefaultPackageNameMenu(msisdn, packageTypeName);
                DataPackage apackage = DefaultpackageNameMenu.getPackage(packageTypeName, packageNameIndex);
                addonprice = apackage.getAddonprice();

                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|inputStep1 " + bundleSession.getInputStep1() + "|inputStep2=" + bundleSession.getInputStep2() + "|inputStep3=" + bundleSession.getInputStep3() + "|inputStep4=" + bundleSession.getInputStep4());

                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|processDataBundleSelectionInput=" + apackage.getPackageName() + " ADDON PRICE " + apackage.getAddonprice());

                if (apackage != null) {

                    ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                    msg = confirmMenu.getString();
                    String contextKey = GetContextKey(ussdSessionid);
                    bundleSession.setStep(4);
                    bundleSession.setSelectedPackage(apackage);
                    context.setAttribute(contextKey, bundleSession);

                } else {
                    msg = DefaultpackageNameMenu.getString();

                }

            }

        } else if (input.equals("#")) {
//            msg = processPackageTypeInput(input,  msisdn,  ussdSessionid, bundleSession, context);
////          msg = displayPackages(context, ussdSessionid, msisdn,ussdCode);
            if (isEligible) {
                PackageTypeMenu packageTypeMenu = new PackageTypeMenu(msisdn);
                msg = packageTypeMenu.getString();
                bundleSession.setStep(1);
            } else {
                DefaultPackageTypeMenu DefaultpackageTypeMenu = new DefaultPackageTypeMenu(msisdn);
                msg = DefaultpackageTypeMenu.getString();
                bundleSession.setStep(1);

            }

        } else {
            //naughty user, he didnt enter anything
            if (isEligible) {
                PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
                msg = packageNameMenu.getString();
            } else {
                DefaultPackageNameMenu DefaultpackageNameMenu = new DefaultPackageNameMenu(msisdn, packageTypeName);
                msg = DefaultpackageNameMenu.getString();
            }

        }

        return msg;
    }

    /*
     ADDON MENU
     */
    public String processAddonDataBundleSelectionInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;
        StringBuilder sb = new StringBuilder();
        //String ussdCode = bundleSession.getUssdCode();
        String packageTypeName = bundleSession.getPackageType();
//        String packageName =bundleSession.getPackageName();
        boolean isEligible = Eligible(msisdn);
        if ((input != null) && (!input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim()))) {

            input = input.trim();
            int packageNameIndex = Integer.parseInt(input);

            inputStep2 = packageNameIndex;

            bundleSession.setInputStep2(inputStep2);

            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|inputStep1 " + bundleSession.getInputStep1() + "|inputStep2=" + bundleSession.getInputStep2() + "|inputStep3=" + bundleSession.getInputStep3() + "|inputStep4=" + bundleSession.getInputStep4());

            int addonprice;
            PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
            DataPackage apackage = packageNameMenu.getPackage(packageTypeName, packageNameIndex);
            DefaultPackageNameMenu DefaultpackageNameMenu = new DefaultPackageNameMenu(msisdn, packageTypeName);
            DataPackage defaultapackage = DefaultpackageNameMenu.getPackage(packageTypeName, packageNameIndex);
            String packageName = defaultapackage.getPackageName();
            addonprice = apackage.getAddonprice();
            if ((apackage != null) && (defaultapackage != null) && (addonprice > 0)) {

                AddonMenu addonMenu = new AddonMenu(apackage);
                msg = addonMenu.getString();
                //if(bundleSession.getInputStep2()==3) SmsNotifcation(msisdn);
                bundleSession.setSelectedPackage(apackage);

                // packagesAddon = packageNameMenu.getPackagesAddon();
                //      MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|processAddonDataBundleSelectionInput="+packagesAddon.length);
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|processAddonDataBundleSelectionInput=" + apackage.getPackageName() + " ADDON PRICE " + apackage.getAddonprice());

                // ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                //msg = confirmMenu.getString();
                String contextKey = GetContextKey(ussdSessionid);
                bundleSession.setStep(3);
                bundleSession.setSelectedPackage(apackage);
                context.setAttribute(contextKey, bundleSession);

                if (input.equals("#")) {
                    ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                    msg = confirmMenu.getString();

                } else {
                    msg = addonMenu.getString();

                }
            } else {
                if (isEligible) {
                    msg = packageNameMenu.getString();
                    ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                    msg = confirmMenu.getString();
                    String contextKey = GetContextKey(ussdSessionid);
                    bundleSession.setStep(4);
                    bundleSession.setSelectedPackage(apackage);
                    context.setAttribute(contextKey, bundleSession);
                } else {
                    if (packageName.equals("BONUS PASS MOMO")) {
                        RequestEnum reqEnum = null;
                        reqEnum = RequestEnum.MOMOBONUSDATA;
                        bundleSession.setSelection(reqEnum);
                        msg = displayMomoPackages(context, ussdSessionid, msisdn, bundleSession.getUssdCode());
                        String contextKey = GetContextKey(ussdSessionid);
                        bundleSession.setStep(5);
                        bundleSession.setPackageType(packageTypeName);
                        context.setAttribute(contextKey, bundleSession);

                    } else if (packageName.equals("Watsp 24h")) {
                        ConfirmMenuRS confirmMenu = new ConfirmMenuRS(apackage);
                        msg = confirmMenu.getString();
                        String contextKey = GetContextKey(ussdSessionid);
                        bundleSession.setStep(4);
                        bundleSession.setSelectedPackage(apackage);
                        context.setAttribute(contextKey, bundleSession);

                    } else if (packageName.equals("Insta 24h")) {
                        ConfirmMenuRS confirmMenu = new ConfirmMenuRS(apackage);
                        msg = confirmMenu.getString();
                        String contextKey = GetContextKey(ussdSessionid);
                        bundleSession.setStep(4);
                        bundleSession.setSelectedPackage(apackage);
                        context.setAttribute(contextKey, bundleSession);

                    } else if (packageName.equals("Watsp 48h")) {
                        ConfirmMenuRS confirmMenu = new ConfirmMenuRS(apackage);
                        msg = confirmMenu.getString();
                        String contextKey = GetContextKey(ussdSessionid);
                        bundleSession.setStep(4);
                        bundleSession.setSelectedPackage(apackage);
                        context.setAttribute(contextKey, bundleSession);

                    } else if (packageName.equals("Insta 48h")) {
                        ConfirmMenuRS confirmMenu = new ConfirmMenuRS(apackage);
                        msg = confirmMenu.getString();
                        String contextKey = GetContextKey(ussdSessionid);
                        bundleSession.setStep(4);
                        bundleSession.setSelectedPackage(apackage);
                        context.setAttribute(contextKey, bundleSession);

                    } else {
                        msg = DefaultpackageNameMenu.getString();
                        ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                        msg = confirmMenu.getString();
                        String contextKey = GetContextKey(ussdSessionid);
                        bundleSession.setStep(4);
                        bundleSession.setSelectedPackage(apackage);
                        context.setAttribute(contextKey, bundleSession);
                    }

                }

            }

//        }else if (input.equals("3")){
//            String str = "Promo disponible sur l app MyMTN. Telechargez gratuitement et profitez en ";
//            return str;
        } else if (input.equals("#")) {
//            msg = processPackageTypeInput(input,  msisdn,  ussdSessionid, bundleSession, context);
////          msg = displayPackages(context, ussdSessionid, msisdn,ussdCode);
            if (isEligible) {
                PackageTypeMenu packageTypeMenu = new PackageTypeMenu(msisdn);
                msg = packageTypeMenu.getString();
                bundleSession.setStep(1);
            } else {
                DefaultPackageTypeMenu DefaultpackageTypeMenu = new DefaultPackageTypeMenu(msisdn);
                msg = DefaultpackageTypeMenu.getString();
                bundleSession.setStep(1);
            }

        } else {
            //naughty user, he didnt enter anything
            if (isEligible) {
                PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
                msg = packageNameMenu.getString();
            } else {
                DefaultPackageNameMenu DefaultpackageNameMenu = new DefaultPackageNameMenu(msisdn, packageTypeName);
                msg = DefaultpackageNameMenu.getString();
            }

        }

        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msg);

        return msg;
    }

    /*
   CONFIRMATION  ---- 669737383
     */
    public String processDataBundleConfirmationInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;

        StringBuilder sb = new StringBuilder();
        //String ussdCode = bundleSession.getUssdCode();
        String packageTypeName = bundleSession.getPackageType();

        input = input.trim();
        int packageNameIndex = Integer.parseInt(input);
        int addonprice;

        inputStep3 = packageNameIndex;

        bundleSession.setInputStep3(inputStep3);

        PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
        DataPackage apackage = packageNameMenu.getPackage(packageTypeName, packageNameIndex);
        addonprice = apackage.getAddonprice();
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|packageNameIndex=" + packageNameIndex + "|packageTypeName=" + packageTypeName + "|processDataBundleConfirmationInput=" + apackage.getPackageName() + " ADDON PRICE " + apackage.getAddonprice());
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|inputStep1 " + bundleSession.getInputStep1() + "|inputStep2=" + bundleSession.getInputStep2() + "|inputStep3=" + bundleSession.getInputStep3() + "|inputStep4=" + bundleSession.getInputStep4());

        DataDA dabase = new DataDA();

        ConfirmMenu confirmMenu = new ConfirmMenu(bundleSession.getSelectedPackage());
        confirmMenu.setOptionNumber(inputStep3);
        msg = confirmMenu.getString();
        String contextKey = GetContextKey(ussdSessionid);
        bundleSession.setStep(4);
        context.setAttribute(contextKey, bundleSession);
        if (bundleSession.getInputStep3() == 3) {
            SmsNotifcation(msisdn);
        }

        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msg);

        return msg;
    }

    /*
     PAYLOAD
     */
    public String payload(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;

        StringBuilder sb = new StringBuilder();
        //String ussdCode = bundleSession.getUssdCode();
        //  String packageTypeName = bundleSession.getPackageType();

        //String ussdCode = bundleSession.getUssdCode();
        DataPackage apackage = bundleSession.getSelectedPackage();
//        String ussdCode = bundleSession.getUssdCode();
        String packageTypeName = bundleSession.getPackageType();
        PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
        if ((input != null) && (!input.trim().equals(""))) {
            input = input.trim();

            if (input.equals(ConfirmMenu.CONFIRMER)) {
                //PROVISION!!!!!!!!!!!!
                //PROVISION!!!!!!!!!!!!
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|" + apackage.getPackageName() + "| Main Account.");

                DataProvisioningEngine provEngine = new DataProvisioningEngine();
                //int provResponse = ProvisioningEngine.RESPONSE_OK;

                if (bundleSession.getInputStep3() == 1) {
                    provEngine.setIsAddon(true);
                } else if (bundleSession.getInputStep3() == 2) {
                    provEngine.setIsAddon(false);
                }

                String ussdCode = bundleSession.getUssdCode();
                ResponseEnum respEnum = provEngine.executeMAPayment(msisdn, ussdSessionid, apackage, refillExternalData, ussdCode);

                if (respEnum == ResponseEnum.SUCCESS) {
                    str = "Cher client votre demande d achat internet a ete envoyee, vous recevrez un message de confirmation. Merci de votre fidelite\n";
                    SmsNotifcation(msisdn);
                    SendPOSSuccessSMS(msisdn, apackage.getPrice());

                } else if (respEnum == ResponseEnum.ERROR) {
                    FeedbackMenu strmsg = new FeedbackMenu();
                    str = strmsg.getErrortxt();
                } else if (respEnum == ResponseEnum.NOT_ALLOWED) {
                    FeedbackMenu strmsg = new FeedbackMenu();
                    str = strmsg.getNotAllowedText();
                } else if (respEnum == ResponseEnum.BALANCEINSUFUSANT) {
                    FeedbackMenu strmsg = new FeedbackMenu();
                    str = strmsg.getInsufficientBalanceText();
//                    SendPOFailSMS(msisdn, apackage.getPrice());
                } else {
                    str = "Il y'a erreur.\n";
                }

//                SendPOSSuccessSMS(msisdn, apackage.getPrice());
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;

            } else if (input.equals(ConfirmMenu.ANNULER)) {// user wants to select again
                
                 
                msg = packageNameMenu.getString();
                bundleSession.setStep(2);
//                
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|" + apackage.getPackageName() + "| Mobile Account.");
//
//                DataProvisioningEngine provEngine = new DataProvisioningEngine();
//                //int provResponse = ProvisioningEngine.RESPONSE_OK;
//                String ussdCode = bundleSession.getUssdCode();
//                if (bundleSession.getInputStep3() == 1) {
//                    provEngine.setIsAddon(true);
//                } else if (bundleSession.getInputStep3() == 2) {
//                    provEngine.setIsAddon(false);
//                }
//
////                if(bundleSession.getInputStep3()==1) {
////                    provEngine.setIsAddon(true);
////                    
////                     String str = "Cher client, vous pouvez acheter le forfait internet 48h via l option Credit. Cette offre sera aussi disponibe via MoMo tres bientot.";
////                     return str;
////                     
////                     
////                }
////                
////                else 
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage, externalData1, ussdCode);
//                String str = "Cher client votre demande d achat internet a ete envoyee, vous recevrez un message de confirmation. Merci de votre fidelite\n";
//                SmsNotifcation(msisdn);
////                SendPOSSuccessSMS(msisdn, apackage.getPrice());
//                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return msg;

            }
            if ((input != null) && (input.trim().equals("#"))) {
                msg = packageNameMenu.getString();
                bundleSession.setStep(2);
            } else {//user entered invalid confirmation input
//                ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
//                msg = confirmMenu.getString();

            }
        } else {
            //naughty user, he didnt enter anything
//            ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
//            msg = confirmMenu.getString();

        }
        return msg;
    }

    protected String displayPackages(ServletContext context, String ussdSessionid, String msisdn, String ussdCode) {
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|Displaying Package list");
        BundleSession bundleSession = (BundleSession) CreateBundleSession();
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        boolean isEligible = Eligible(msisdn);
        if (obj != null) {
            context.removeAttribute(contextKey);
        }
        bundleSession.setUssdSessionId(ussdSessionid);
        bundleSession.setUssdCode(ussdCode);
        bundleSession.setStep(1);

        context.setAttribute(contextKey, bundleSession);
        if (isEligible) {
            PackageTypeMenu menu = new PackageTypeMenu(msisdn);
            String str = menu.getString();
            return str;
        } else {
            DefaultPackageTypeMenu menu = new DefaultPackageTypeMenu(msisdn);
            String str = menu.getString();
            return str;
        }

    }

    //Display MomoBonus Packages
    protected String displayMomoPackages(ServletContext context, String ussdSessionid, String msisdn, String ussdCode) {
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|Displaying Package list");
        BundleSession bundleSession = (BundleSession) CreateBundleSession();
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        boolean isEligible = Eligible(msisdn);
        if (obj != null) {
            context.removeAttribute(contextKey);
        }
        bundleSession.setUssdSessionId(ussdSessionid);
        bundleSession.setUssdCode(ussdCode);
        bundleSession.setStep(1);

        context.setAttribute(contextKey, bundleSession);
        if (isEligible) {
            PackageTypeMenu menu = new PackageTypeMenu(msisdn);
            String str = menu.getString();
            return str;
        } else {
            MomoPackageTypeMenu menu = new MomoPackageTypeMenu(msisdn);
            String str = menu.getString();
            return str;
        }

    }

    //Process 
    public String processMoMoDataBundleSelectionInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {

        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|Displaying Package list|Session " + bundleSession.getSelection() + "");

        String msg = null;

        if ((input != null) && (!input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim()))) {

            input = input.trim();

            RequestEnum reqEnum = null;

//            if (input.equals("1")) {
//                reqEnum = RequestEnum.MOMOPASSJOUR;
//                 msg = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//            } else if (input.equals("2")) {
//                
//                 msg = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                reqEnum = RequestEnum.MOMOPASS48H;
//            } else if (input.equals("3")) {
//                 msg = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                reqEnum = RequestEnum.MOMOPASSWEEK;
//            } else if (input.equals("4")) {
//                 msg = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                reqEnum = RequestEnum.MOMOPASSMOIS;
//            } else if (input.equals("#")) {
//                 msg = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
////                msg = displayMomoPackages(context, ussdSessionid, msisdn, msisdn);
//            }
//            bundleSession.setSelection(reqEnum);
//            bundleSession.setStep(6);
            msg =  msg = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
        } else {
            msg =  msg = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
        }
        return msg;
    }

    // Process Mon Compte Menu
    //Process 
    public String processMonComptenput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {

        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|Displaying Mon Compte list|Session " + bundleSession.getSelection() + "");

        String msg = null;

        if ((input != null) && (!input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim()))) {
            RequestEnum reqEnum = bundleSession.getSelection();
            MLogger.Log(this, LogLevel.DEBUG, "RequestEnum " + reqEnum.toString());
            input = input.trim();

            if (input.equals("1")) {
                reqEnum = RequestEnum.PASSENCOURS;
            } else if (input.equals("2")) {
                reqEnum = RequestEnum.PASSENATTENTE;
            } else if (input.equals("3")) {
                reqEnum = RequestEnum.PROMO;
            } else if (input.equals("4")) {
                reqEnum = RequestEnum.PAYG;
            } else if (input.equals("#")) {
                msg = displayPackages(context, ussdSessionid, msisdn, msisdn);
            }
            bundleSession.setSelection(reqEnum);
            bundleSession.setStep(9);
            msg = processMonCompteConfirmInput(input, msisdn, ussdSessionid, bundleSession, context);
        } else {

            msg = displayMonCompteMenu(input, msisdn, ussdSessionid, bundleSession, context);
        }
        MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input " + input);
        return msg;
    }

//Process 
    public String processMoMoDataBundleConfirmInput(String input, String msisdn, String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
       
        if((input != null) && ( !input.trim().equals("")) && (input.trim().length() >= 1) && (StringUtils.isNumeric(input.trim())) ){{
            input =  input.trim();
             RequestEnum reqEnum = bundleSession.getSelection();
             MLogger.Log(this, LogLevel.DEBUG, "RequestEnum "+reqEnum.toString());
             DataProvisioningEngine provEngine = new DataProvisioningEngine();
             int packageNameIndex = Integer.parseInt(input);
             if(reqEnum == RequestEnum.MOMOPASSJOUR && input.equals("1")){
//                bundleSession.setPackageType("Pass jour");
//                bundleSession.setPackageName("40MB + 10MB");
//                bundleSession.setDatareq("BU34");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }else if(reqEnum == RequestEnum.MOMOPASSJOUR && input.equals("2")){
//                bundleSession.setPackageType("Pass jour");
//                bundleSession.setPackageName("120MB + 30MB");
//                bundleSession.setDatareq("BU44");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }else if(reqEnum == RequestEnum.MOMOPASSJOUR && input.equals("3")){
//                bundleSession.setPackageType("Pass jour");
//                bundleSession.setPackageName("250MB + 125MB");
//                bundleSession.setDatareq("BU66");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
               str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASSJOUR && input.equals("4")){ 
//                bundleSession.setPackageType("Pass jour");
//                bundleSession.setPackageName("500MB + 250MB");
//                bundleSession.setDatareq("MO03");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             } else if(reqEnum == RequestEnum.MOMOPASS48H && input.equals("1")){
//                bundleSession.setPackageType("Pass 48H");
//                bundleSession.setPackageName("60MB + 15MB");
//                bundleSession.setDatareq("MO12");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASS48H && input.equals("2")){
//                bundleSession.setPackageType("Pass 48H");
//                bundleSession.setPackageName("140MB + 35MB");
//                bundleSession.setDatareq("BU51");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASS48H && input.equals("3")){
//                bundleSession.setPackageType("Pass 48H");
//                bundleSession.setPackageName("300MB + 150MB");
//                bundleSession.setDatareq("BU47");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASS48H && input.equals("4")){
//                bundleSession.setPackageType("Pass 48H");
//                bundleSession.setPackageName("500MB + 250MB");
//                bundleSession.setDatareq("BU68");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
              else if(reqEnum == RequestEnum.MOMOPASSWEEK && input.equals("1")){
//                bundleSession.setPackageType("Pass Semaine");
//                bundleSession.setPackageName("650MB + 325MB");
//                bundleSession.setDatareq("MO01");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
              else if(reqEnum == RequestEnum.MOMOPASSWEEK && input.equals("2")){
//                bundleSession.setPackageType("Pass Semaine");
//                bundleSession.setPackageName("1.3GB + 650MB");
//                bundleSession.setDatareq("BU10");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASSWEEK && input.equals("3")){
//                bundleSession.setPackageType("Pass Semaine");
//                bundleSession.setPackageName("2.5GB + 1.25GB");
//                bundleSession.setDatareq("MO18");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
              else if(reqEnum == RequestEnum.MOMOPASSMOIS && input.equals("1")){
//                bundleSession.setPackageType("Pass Mois");
//                bundleSession.setPackageName("2.5GB + 1.25GB");
//                bundleSession.setDatareq("MO09");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str = "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASSMOIS && input.equals("2")){
//                bundleSession.setPackageType("Pass Mois");
//                bundleSession.setPackageName("5GB + 2.5GB");
//                bundleSession.setDatareq("MO15");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASSMOIS && input.equals("3")){
//                bundleSession.setPackageType("Pass Mois");
//                bundleSession.setPackageName("7.5GB + 3.75GB");
//                bundleSession.setDatareq("MO21");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                 str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASSMOIS && input.equals("4")){
//                bundleSession.setPackageType("Pass Mois");
//                bundleSession.setPackageName("15GB + 7.5GB");
//                bundleSession.setDatareq("MO10");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASSMOIS && input.equals("5")){
//                bundleSession.setPackageType("Pass Mois");
//                bundleSession.setPackageName("30GB + 15GB");
//                bundleSession.setDatareq("MO22");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASSMOIS && input.equals("6")){
//                bundleSession.setPackageType("Pass Mois");
//                bundleSession.setPackageName("50GB + 25GB");
//                bundleSession.setDatareq("MO20");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
                str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             else if(reqEnum == RequestEnum.MOMOPASSMOIS && input.equals("7")){
//                bundleSession.setPackageType("Pass Mois");
//                bundleSession.setPackageName("100GB + 50GB");
//                bundleSession.setDatareq("MO17");
//                MomoPackageNameMenu packageNameMenu = new MomoPackageNameMenu(msisdn, bundleSession.getPackageType());  
//                DataPackage apackage = packageNameMenu.getMomoPassPackage(bundleSession.getPackageName(), packageNameIndex);;
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list|Request "+bundleSession.getPackageType()+"|PackageName:"+bundleSession.getPackageName()+"|RefilId"+bundleSession.getDatareq());
//                String contextKey = GetContextKey(ussdSessionid);
//                context.setAttribute(contextKey, bundleSession);
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage,externalData1,bundleSession.getUssdCode());
               str =  "Desormais Pour les Bonus MoMo \n Tapez *440*3*1# \n";
//                SmsNotifcation(msisdn);
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;
                 
             }
             
        }
        return msg;
    }
      return msg;  
 }
    // Process Mon Compte Item
    public String processMonCompteConfirmInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;
        DataDA dbClient = new DataDA();
        StringBuilder sb = new StringBuilder();

        return msg;
    }

    /*
     PAYLOAD MOMO BUNDLES
     */
    public String payloadMomo(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;

        StringBuilder sb = new StringBuilder();
        //String ussdCode = bundleSession.getUssdCode();
        //  String packageTypeName = bundleSession.getPackageType();

        //String ussdCode = bundleSession.getUssdCode();
        DataPackage apackage = bundleSession.getSelectedPackage();
//        String ussdCode = bundleSession.getUssdCode();
        String packageTypeName = bundleSession.getPackageType();
        PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
        if ((input != null) && (!input.trim().equals(""))) {
            input = input.trim();

            if (input.equals(ConfirmMenu.CONFIRMER)) {
                //PROVISION!!!!!!!!!!!!
                //PROVISION!!!!!!!!!!!!
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|" + apackage.getPackageName() + "| Main Account.");

                DataProvisioningEngine provEngine = new DataProvisioningEngine();
                //int provResponse = ProvisioningEngine.RESPONSE_OK;

                if (bundleSession.getInputStep3() == 1) {
                    provEngine.setIsAddon(true);
                } else if (bundleSession.getInputStep3() == 2) {
                    provEngine.setIsAddon(false);
                }

                String ussdCode = bundleSession.getUssdCode();
                ResponseEnum respEnum = provEngine.executeMAPayment(msisdn, ussdSessionid, apackage, refillExternalData, ussdCode);
                if (respEnum == ResponseEnum.SUCCESS) {
                    str = "Cher client votre demande d achat internet a ete envoyee, vous recevrez un message de confirmation. Merci de votre fidelite\n";
                    SmsNotifcation(msisdn);
                    SendPOSSuccessSMS(msisdn, apackage.getPrice());

                } else if (respEnum == ResponseEnum.ERROR) {
                    FeedbackMenu strmsg = new FeedbackMenu();
                    str = strmsg.getErrortxt();
                } else if (respEnum == ResponseEnum.NOT_ALLOWED) {
                    FeedbackMenu strmsg = new FeedbackMenu();
                    str = strmsg.getNotAllowedText();
                } else if (respEnum == ResponseEnum.BALANCEINSUFUSANT) {
                    FeedbackMenu strmsg = new FeedbackMenu();
                    str = strmsg.getInsufficientBalanceText();
//                    SendPOFailSMS(msisdn, apackage.getPrice());
                } else {
                    str = "Il y'a erreur.\n";
                }
                this.FreeFlow = "FB";
                this.Cleanup(context, ussdSessionid);
                return str;

            } else if (input.equals(ConfirmMenu.ANNULER)) {// user wants to select again
                
                msg = packageNameMenu.getString();
                bundleSession.setStep(2);
//                        
//                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|" + apackage.getPackageName() + "| Mobile Account.");
//
//                DataProvisioningEngine provEngine = new DataProvisioningEngine();
//                //int provResponse = ProvisioningEngine.RESPONSE_OK;
//                String ussdCode = bundleSession.getUssdCode();
//                if (bundleSession.getInputStep3() == 1) {
//                    provEngine.setIsAddon(true);
//                } else if (bundleSession.getInputStep3() == 2) {
//                    provEngine.setIsAddon(false);
//                }
//
////                if(bundleSession.getInputStep3()==1) {
////                    provEngine.setIsAddon(true);
////                    
////                     String str = "Cher client, vous pouvez acheter le forfait internet 48h via l option Credit. Cette offre sera aussi disponibe via MoMo tres bientot.";
////                     return str;
////                     
////                     
////                }
////                
////                else 
//                msg = provEngine.executeMoMoPayment(msisdn, ussdSessionid, apackage, externalData1, ussdCode);
//                String str = "Cher client votre demande d achat internet a ete envoyee, vous recevrez un message de confirmation. Merci de votre fidelite\n";
//                SmsNotifcation(msisdn);
//                SendPOSSuccessSMS(msisdn, apackage.getPrice());
//                this.FreeFlow = "FB";
//                this.Cleanup(context, ussdSessionid);
                return msg;

            }
            if ((input != null) && (input.trim().equals("#"))) {
                msg = packageNameMenu.getString();
                bundleSession.setStep(2);
            } else {//user entered invalid confirmation input
//                ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
//                msg = confirmMenu.getString();

            }
        } else {
            //naughty user, he didnt enter anything
//            ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
//            msg = confirmMenu.getString();

        }
        return msg;
    }

    //
    public String displaySubMenu(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;
        String req = null;
        MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input " + input);

        RequestEnum reqEnum = bundleSession.getSelection();
        MLogger.Log(this, LogLevel.DEBUG, "RequestEnum " + reqEnum.toString());
        MomoSUBMenu subMenu = new MomoSUBMenu(reqEnum);
        msg = subMenu.getString();
        bundleSession.setStep(6);
        bundleSession.setSelection(reqEnum);
        String contextKey = GetContextKey(ussdSessionid);
        context.setAttribute(contextKey, bundleSession);
        return msg;
    }

    public String displayMonCompteMenu(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;
        String req = null;
        MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input " + input);

        RequestEnum reqEnum = bundleSession.getSelection();
        MLogger.Log(this, LogLevel.DEBUG, "RequestEnum " + reqEnum.toString());
        MenuMonCompte Menu = new MenuMonCompte(reqEnum);
        msg = Menu.getString();
        bundleSession.setStep(9);
        bundleSession.setSelection(reqEnum);
        String contextKey = GetContextKey(ussdSessionid);
        context.setAttribute(contextKey, bundleSession);
        return msg;
    }

    // Process ShortCut
    private String processShortCut(String packageTypeIndex, String packageNameIndex, String msisdn, String ussdSessionid, ServletContext context) {
        String msg = null;
        int index = Integer.parseInt(packageTypeIndex);
        PackageTypeMenu packageTypeMenu = new PackageTypeMenu(msisdn);
        String packageTypeName = packageTypeMenu.getPackageType(index);
        MLogger.Log(this, LogLevel.DEBUG, "PackageType " + packageTypeIndex + " and the PackageName " + packageTypeName);
        BundleSession bundleSession = new BundleSession();
        bundleSession.setStep(2);
        bundleSession.setPackageType(packageTypeName);
        DataPackage apackage = new DataPackage();
        bundleSession.setUssdSessionId(ussdSessionid);
        msg = processDataBundleSelectionInput(packageNameIndex, msisdn, ussdSessionid, bundleSession, context);
        return msg;
    }

    // Process RS Shortcut
    private String processAddonShortCut(String packageTypeIndex, String packageNameIndex, String msisdn, String ussdSessionid, ServletContext context) {
        String msg = null;
        int index = Integer.parseInt(packageTypeIndex);
        PackageTypeMenu packageTypeMenu = new PackageTypeMenu(msisdn);
        String packageTypeName = packageTypeMenu.getPackageType(index);
        MLogger.Log(this, LogLevel.DEBUG, "PackageType " + packageTypeIndex + " and the PackageName " + packageTypeName);
        BundleSession bundleSession = new BundleSession();
        bundleSession.setStep(1);
        bundleSession.setPackageType(packageTypeName);
        DataPackage apackage = new DataPackage();
        bundleSession.setUssdSessionId(ussdSessionid);
//        msg = processDataBundleSelectionInput(packageNameIndex, msisdn, ussdSessionid,bundleSession,context);
        msg = processAddonDataBundleSelectionInput(packageNameIndex, msisdn, ussdSessionid, bundleSession, context);
        return msg;
    }

    public String processBundlesMomo(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;
        StringBuilder sb = new StringBuilder();
        //String ussdCode = bundleSession.getUssdCode();
        String packageTypeName = bundleSession.getPackageType();
        boolean isEligible = Eligible(msisdn);
        if ((input != null) && (!input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim()))) {

            input = input.trim();
            int packageNameIndex = Integer.parseInt(input);

            inputStep2 = packageNameIndex;

            bundleSession.setInputStep2(inputStep2);

            MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|inputStep1 " + bundleSession.getInputStep1() + "|inputStep2=" + bundleSession.getInputStep2() + "|inputStep3=" + bundleSession.getInputStep3() + "|inputStep4=" + bundleSession.getInputStep4());

            int addonprice;
            PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
            DataPackage apackage = packageNameMenu.getPackage(packageTypeName, packageNameIndex);
            DefaultPackageNameMenu DefaultpackageNameMenu = new DefaultPackageNameMenu(msisdn, packageTypeName);
            DataPackage defaultapackage = DefaultpackageNameMenu.getPackage(packageTypeName, packageNameIndex);
            addonprice = apackage.getAddonprice();
            if (packageTypeName.equals("BONUS MoMo")) {

                MomoPackageNameMenu addonMenu = new MomoPackageNameMenu(msisdn, packageTypeName);
                msg = addonMenu.getString();
                //if(bundleSession.getInputStep2()==3) SmsNotifcation(msisdn);
                bundleSession.setSelectedPackage(apackage);

                // packagesAddon = packageNameMenu.getPackagesAddon();
                //      MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|processAddonDataBundleSelectionInput="+packagesAddon.length);
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|processAddonDataBundleSelectionInput=" + apackage.getPackageName() + " ADDON PRICE " + apackage.getAddonprice());

                // ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                //msg = confirmMenu.getString();
                String contextKey = GetContextKey(ussdSessionid);
                bundleSession.setStep(7);
                bundleSession.setSelectedPackage(apackage);
                context.setAttribute(contextKey, bundleSession);

                if (input.equals("#")) {
                    ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                    msg = confirmMenu.getString();

                } else {
                    msg = addonMenu.getString();

                }
            } else {
                if (isEligible) {
                    msg = packageNameMenu.getString();
                    ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                    msg = confirmMenu.getString();
                    String contextKey = GetContextKey(ussdSessionid);
                    bundleSession.setStep(4);
                    bundleSession.setSelectedPackage(apackage);
                    context.setAttribute(contextKey, bundleSession);
                } else {
                    msg = DefaultpackageNameMenu.getString();
                    ConfirmMenu confirmMenu = new ConfirmMenu(apackage);
                    msg = confirmMenu.getString();
                    String contextKey = GetContextKey(ussdSessionid);
                    bundleSession.setStep(4);
                    bundleSession.setSelectedPackage(apackage);
                    context.setAttribute(contextKey, bundleSession);
                }

            }

//        }else if (input.equals("3")){
//            String str = "Promo disponible sur l app MyMTN. Telechargez gratuitement et profitez en ";
//            return str;
        } else if (input.equals("#")) {
//            msg = processPackageTypeInput(input,  msisdn,  ussdSessionid, bundleSession, context);
////          msg = displayPackages(context, ussdSessionid, msisdn,ussdCode);
            if (isEligible) {
                PackageTypeMenu packageTypeMenu = new PackageTypeMenu(msisdn);
                msg = packageTypeMenu.getString();
                bundleSession.setStep(1);
            } else {
                DefaultPackageTypeMenu DefaultpackageTypeMenu = new DefaultPackageTypeMenu(msisdn);
                msg = DefaultpackageTypeMenu.getString();
                bundleSession.setStep(1);
            }

        } else {
            //naughty user, he didnt enter anything
            if (isEligible) {
                PackageNameMenu packageNameMenu = new PackageNameMenu(msisdn, packageTypeName);
                msg = packageNameMenu.getString();
            } else {
                DefaultPackageNameMenu DefaultpackageNameMenu = new DefaultPackageNameMenu(msisdn, packageTypeName);
                msg = DefaultpackageNameMenu.getString();
            }

        }

        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msg);

        return msg;
    }

    protected String displayPaygMainMenu(ServletContext context, String ussdSessionid, String msisdn) {
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:" + msisdn + "|Session " + ussdSessionid + "|Displaying Package list");
        BundleSession bundleSession = new BundleSession();
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);

        if (obj != null) {
            context.removeAttribute(contextKey);
        }
        bundleSession.setUssdSessionId(ussdSessionid);

        bundleSession.setStep(10);

        context.setAttribute(contextKey, bundleSession);
        PAYGMenu menu = new PAYGMenu();
        String str = menu.getString();

        return str;
    }

    public String processPAYGMenuInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession, ServletContext context) {
        String msg = null;
        if ((input != null) && (!input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim()))) {
            input = input.trim();
            StringBuilder sb = new StringBuilder();
            RequestEnum reqEnum = null;

            if (input.equals("1")) {
//               SmsNotifcation(msisdn);
                reqEnum = RequestEnum.DECOUVRIR;
                sb.append("Naviguez sur Internet  a seulement 0.05F/kbit . MTN Vous remercie\n");
                sb.append("\n");
                sb.append("\n");
                sb.append("#:Reour\n");
                input = "1";
                msg = sb.toString();
                if (input.equals("#")) {
                    msg = displayPaygMainMenu(context, ussdSessionid, msisdn);
                }
            }

            if (input.equals("2")) {
                reqEnum = RequestEnum.ACTIVATE;
                sb.append("Confirmez vous l activation de MTN PAYG?\n");
                sb.append("\n");
                sb.append("1:Confirmer\n");
                sb.append("2:Annuler\n");
                sb.append("Repondez");
                input = "2";
                msg = sb.toString();

            } else if (input.equals("3")) {
                reqEnum = RequestEnum.DEACTIVATE;
                sb.append("Confirmez vous la desactivation de MTN PAYG xtra?\n");
                sb.append("\n");
                sb.append("1:Confirmer\n");
                sb.append("2:Annuler\n");
                sb.append("Repondez");
                input = "3";
                msg = sb.toString();

            }

            //if(!input.equals("5") && !input.equals("6")){
            PaygConfirmMenu confirmMenu = new PaygConfirmMenu(reqEnum);
            msg = confirmMenu.getString();

            bundleSession.setStep(2);
            bundleSession.setSelection(reqEnum);
            String contextKey = GetContextKey(ussdSessionid);
            context.setAttribute(contextKey, bundleSession);
            //}                             
        } else {
            //naughty user, he entered wrong information
            msg = displayPaygMainMenu(context, ussdSessionid, msisdn);
        }
        return msg;
    }
    // Confirm Payg 

    protected AbstractBundleSession CreateBundleSession() {
        return (AbstractBundleSession) new BundleSession();
    }

    protected AbstractMainMenu CreateMainMenu(String msisdn) {
        //return (AbstractMainMenu) new PackageTypeMenu(msisdn);
        return null;
    }

    private boolean isMsisdn(String input) {
        boolean ok = false;
        if ((input != null) && (!input.trim().equals(""))) {
            input = input.trim();
            //664222545
            if (input.length() == 9) {
                if (StringUtils.isNumeric(input)) {
                    ok = true;
                }
            }
        }
        return ok;
    }

    private void SmsNotifcation(String msisdn) {
        String msisdnStr = msisdn;
        msisdnStr = "224" + msisdnStr;// "224664222412";
        String msg = "MTN Yello TV! Plus 500 films & Series, + 20 chaines de Télé EN DIRECT sur votre smartphone et SANS CONSOMMER VOTRE PASS! Telechargez sur bit.ly/39Xwnu6  .";
        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("YelloTV", msisdnStr, msg);

    }

    private boolean Eligible(String msisdn) {
        boolean resp = false;
        DataDA dbClient = new DataDA();
        GoodYearMsisdn dbBs = dbClient.getEligibleMsisdn(msisdn);
        if (dbBs != null) {
            resp = true;
        }
        return resp;
    }
//    private boolean BonusMomoPackage(String packagetype_name){
//        packagetype_name= "BONUS MoMo";
//        boolean resp = false;
//        DataDA dbClient = new DataDA();
//        TypePackage mom = dbClient.getBonusMomoPackagetype(packagetype_name);
//        if(mom != null)
//            resp = true;
//        return resp;
//    }

    private void SendPOSSuccessSMS(String msisdn, int price) {

        String msg = "Vous avez beneficiez de  " + price + " FG de connexion Internet. Tapez *223# pour le solde.";

        String msisdnStr = msisdn;
        //msisdn = "664222545";
        if (!msisdnStr.startsWith("224")) {
            msisdnStr = "224" + msisdnStr;
        }

        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("Internet", msisdnStr, msg);

    }

    private void SendPOFailSMS(String msisdn, int price) {

        String msg = "Vous n avez pas assez de credit pour acheter ce Pass de  " + price + " FG , Tapez *200# pour emprunter un pass internet";

        String msisdnStr = msisdn;
        //msisdn = "664222545";
        if (!msisdnStr.startsWith("224")) {
            msisdnStr = "224" + msisdnStr;
        }

        SMSClient smsClient = new SMSClient();
        smsClient.sendSMS("Internet", msisdnStr, msg);

    }

    public static String humanReadableByteCountSI(double bytes) {
        if (-1024 < bytes && bytes < 1000) {
            return bytes + " B";
        }
        CharacterIterator ci = new StringCharacterIterator("kMGTPE");
        while (bytes <= -999_950 || bytes >= 999_950) {
            bytes /= 1024;
            ci.next();
        }
        return String.format("%.1f %cB", bytes / 1024.0, ci.current());
    }

}
