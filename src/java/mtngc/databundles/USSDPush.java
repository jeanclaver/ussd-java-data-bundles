/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
//import javax.servlet.http.HttpSession ;
import javax.servlet.ServletContext ;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import java.net.URLEncoder;

/**
 *
 * @author JcMoutoh
 */
public class USSDPush {
    public void test(HttpServletRequest request, HttpServletResponse response){
    
        StringBuilder msg = new StringBuilder();
        msg.append("Please select option\n");
        msg.append("1. Accept\n");
        msg.append("2. Cancel");
        String msgStr = msg.toString();
        
        String ussdCode = "605";
        
        USSDPushResponse resp =  push("224664222544", msgStr, ussdCode);        
        try{
            PrintWriter out = response.getWriter();
            out.append(resp.getMessage());           
            out.close(); 
        }catch (Exception e) {
            MLogger.Log(this,e);
        }
    }
    
    public USSDPushResponse push(String msisdn, String pushMessage, String ussdCode){
         USSDPushResponse ret = new USSDPushResponse();
    
        StringBuilder sb1 = new StringBuilder();        
        
        try {
            String msgStr = URLEncoder.encode(pushMessage, "UTF-8");
            
            //String url = "http://10.12.255.4:9001/ussdhttpquery/qs?USER_ID=push_cp&PUSH_TEXT="+msgStr+"&PASSWORD=Root@123&SERVICE_CODE=103&MSISDN="+msisdn+"&PUSH_TYPE=3";
            String url = "http://10.12.255.4:9001/ussdhttpquery/qs?USER_ID=push_cp&PUSH_TEXT="+msgStr+"&PASSWORD=Root@123&SERVICE_CODE="+ussdCode+"&MSISDN="+msisdn+"&PUSH_TYPE=3";
        
            String logStr = "Sending 'GET' request to URL : " + url;
            MLogger.Log(this, LogLevel.ALL, logStr);
            
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            String USER_AGENT = "Mozilla/5.0";
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            if (responseCode == 200)
                ret.setSuccess(true);

            
            logStr = "Response Code : " + responseCode;
            MLogger.Log(this, LogLevel.ALL, logStr);
            sb1.append(logStr+"\n");
            
            logStr = "Response Message : " + con.getResponseMessage();
            MLogger.Log(this, LogLevel.ALL, logStr);
            sb1.append(logStr+"\n");
            
            
            Map<String,List<String>> l = con.getHeaderFields(); 

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer sb = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                    sb.append(inputLine);
                    MLogger.Log(this, LogLevel.ALL, inputLine);
            }
            sb1.append(sb.toString());
            ret.setMessage(sb1.toString());
            in.close();
        }catch (Exception e) {
            MLogger.Log(this,e);
        }
        
        return ret;
                    
    
    }
    
    public class USSDPushResponse{
        private boolean success;
        private String message;

        /**
         * @return the success
         */
        public boolean isSuccess() {
            return success;
        }

        /**
         * @param success the success to set
         */
        public void setSuccess(boolean success) {
            this.success = success;
        }

        /**
         * @return the message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message the message to set
         */
        public void setMessage(String message) {
            this.message = message;
        }
    
    }
    
}
