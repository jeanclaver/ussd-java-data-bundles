/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.databundles.momo;

import dbaccess.dbsrver.datareseller.DataPackage;
import java.io.Serializable;

/**
 *
 * @author JcMoutoh
 */
    public class MoMoRequest implements Serializable {
        private String msisdn;
        private String transactionId;
        private DataPackage option;
        private String USSDCode;
        private int step;
        private boolean isAddon;
        

        /**
         * @return the option
         */
        public DataPackage getOption() {
            return option;
        }

        /**
         * @param option the option to set
         */
        public void setOption(DataPackage option) {
            this.option = option;
        }

        /**
         * @return the msisdn
         */
        public String getMsisdn() {
            return msisdn;
        }

        /**
         * @param msisdn the msisdn to set
         */
        public void setMsisdn(String msisdn) {
            this.msisdn = msisdn;
        }

        /**
         * @return the transactionId
         */
        public String getTransactionId() {
            return transactionId;
        }

        /**
         * @param transactionId the transactionId to set
         */
        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

    /**
     * @return the USSDCode
     */
    public String getUSSDCode() {
        return USSDCode;
    }

    /**
     * @param USSDCode the USSDCode to set
     */
    public void setUSSDCode(String USSDCode) {
        this.USSDCode = USSDCode;
    }
     /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param step the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }
    
     /**
     * @return the ussdSessionId
     */
    public boolean getisAddon() {
        return isAddon;
    }

    /**
     * @param ussdSessionId the step to set
     */
    public void setisAddon(boolean isAddon) {
        this.isAddon = isAddon;
    }
    
    }
